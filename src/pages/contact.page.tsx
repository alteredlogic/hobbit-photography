import React from 'react'
import { SplitView } from '../components/templates/split-view.template'
import { PageDescription } from '../components/organisms/page-description.organism'

export const ContactPage = () => (
  <SplitView
    leftView={
      <PageDescription
        backgroundImageUrl="https://picsum.photos/1000/1000"
        title="Contact Us"
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar."
      />
    }
  />
)
