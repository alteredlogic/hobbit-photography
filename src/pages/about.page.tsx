import React, { ReactElement } from 'react'
import { SplitView } from '../components/templates/split-view.template'
import { PageDescription } from '../components/organisms/page-description.organism'
import { MyStory } from '../components/organisms/my-story.organism'

export const AboutPage = () => (
  <SplitView
    leftView={
      <PageDescription
        backgroundImageUrl="https://picsum.photos/1000/1000"
        title="About Us"
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar."
      />
    }
    rightView={
      <MyStory
        title="MY LITTLE STORY"
        subTitle="Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa"
        sectionNumber="1"
        storyText="Cras mattis iudicium purus sit amet fermentum at nos hinc posthac, sitientis piros afros. Lorem ipsum dolor sit amet, consectetur adipisici elit, petierunt uti sibi concilium totius Galliae in diem sed eius mod tempor incidunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus et netus piros labore et dolore magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra."
        storySubText="Praesent nec leo venenatis elit semper aliquet id ac enim. Maecenas nec mi leo. Etiam venenatis ut dui non hendrerit. Integer dictum, diam vitae blandit accumsan, dolor odio tempus arcu, vel ultrices nisi nibh vitae ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi varius lacinia vestibulum. Aliquam lobortis facilisis tellus, in facilisis ex vehicula ac. In malesuada quis turpis vel viverra."
        fact="interesting"
        factTitle="interesting title"
      />
    }
  />
)
