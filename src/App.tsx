import React, { useState } from 'react'
import styled from '@emotion/styled'
import { ThemeProvider } from 'emotion-theming'
import { theme } from './utils/theme'
import { Header } from './components/organisms/header.organism'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { AboutPage } from './pages/about.page'
import { PortfolioPage } from './pages/portfolio.page'
import { HomePage } from './pages/home.page'
import { ContactPage } from './pages/contact.page'
import { NewsPage } from './pages/news.page'
import { SideBar } from './components/organisms/sidebar.organism'

const SiteWrapper = styled.main`
  height: 100%;
  padding-top: 90px;
`
const App: React.FC = () => {
  const [isOpen, setIsOpen] = useState(false)
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <SiteWrapper>
          <Header setIsOpen={setIsOpen} isOpen={isOpen} />
          <SideBar isOpen={isOpen} />
          <Switch>
            <Route path="/" exact>
              <HomePage />
            </Route>
            <Route path="/about">
              <AboutPage />
            </Route>
            <Route path="/portfolio">
              <PortfolioPage />
            </Route>
            <Route path="/news">
              <NewsPage />
            </Route>
            <Route path="/contact">
              <ContactPage />
            </Route>
          </Switch>
        </SiteWrapper>
      </Router>
    </ThemeProvider>
  )
}

export default App
