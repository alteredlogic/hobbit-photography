import React from 'react'
import styled from '@emotion/styled'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  fab,
  faTwitter,
  faFacebookF,
  faInstagram
} from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

library.add(fab, faTwitter, faFacebookF)

const SocialLink = styled.a`
  width: 36px;
  height: 36px;
  color: white;
  font-size: 11px;
  border-radius: 50%;
  background-color: #292929;
  display: flex;
  justify-content: center;
  line-height: 36px;
  margin: 0px 2px;
`
interface IProps {
  icon: string
  url: string
}

export const SocialIcon = (props: IProps) => {
  const iconObject: { [key: string]: any } = {
    twitter: faTwitter,
    facebook: faFacebookF,
    instagram: faInstagram
  }
  const icon = iconObject[props.icon]
  return icon ? (
    <SocialLink href={props.url} target="__blank">
      <div>
        {' '}
        <FontAwesomeIcon icon={icon} />
      </div>
    </SocialLink>
  ) : null
}
