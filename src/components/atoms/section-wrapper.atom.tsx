import styled from '@emotion/styled'

export const SectionWrapper = styled.div`
  margin-bottom: 50px;
  padding-bottom: 30px;
  border-bottom: 1px solid rgba(238, 238, 238, 0.1);
`
