import styled from '@emotion/styled'
import Button from 'react-bootstrap/Button'

export const PrimaryButton = styled(Button)`
  border: solid 2px #a6735e;
  color: #a6735e;
  cursor: pointer;
  display: inline-block;
  font-family: 'pitch', serif;
  font-size: 14px;
  letter-spacing: 0.12em;
  line-height: 1.5;
  padding: 18px 46px;
  position: relative;
  text-transform: uppercase;
  transition: color 0.3s cubic-bezier(0.55, 0, 0.1, 1),
    border-color 0.3s cubic-bezier(0.55, 0, 0.1, 1);
  background: transparent;
  :hover {
    border: solid 2px white;
    color: white;
    background: transparent;
  }
`

export const SecondaryButton = styled(PrimaryButton)`
  background: #000;
`
