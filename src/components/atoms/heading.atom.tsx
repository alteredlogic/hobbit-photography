import styled from '@emotion/styled'

export const H2 = styled.h2`
  font-family: sans-serif;
  text-align: left;
  font-size: 38px;
  text-transform: uppercase;
  font-weight: normal;
  width: 100%;
  padding-bottom: 15px;
  color: #fff;
  letter-spacing: 0.15em;
`

export const H3 = styled(H2)`
  font-size: 24px;
`
export const H4 = styled.h4`
  font-family: 'pitch', serif;
  color: #a6735e;
  max-width: 400px;
  text-align: left;
  font-size: 13px;
`
