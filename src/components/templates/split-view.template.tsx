import React, { SFC, ReactElement } from 'react'
import styled from '@emotion/styled'
import { Row, Col } from 'react-bootstrap'

interface IProps {
  leftView: ReactElement
  rightView?: ReactElement
}

const Column = styled(Col)`
  position: relative;
  height: 100%;
  z-index: 0;
`

export const SplitView: SFC<IProps> = ({
  leftView,
  rightView
}): ReactElement => (
  <>
    <Row>
      <Column>{leftView}</Column>
      <Column>{rightView}</Column>
    </Row>
  </>
)
