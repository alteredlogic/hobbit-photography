import React from 'react'
import styled from '@emotion/styled'
import { H3, H4 } from '../atoms/heading.atom'
import { Form, FormControl } from 'react-bootstrap'
import { PrimaryButton } from '../atoms/button.atom'
import { SectionWrapper } from '../atoms/section-wrapper.atom'

const Input = styled(FormControl)`
  height: 55px;
  border-radius: 0;
  margin: 30px 0px 10px 0px;
  border: 1px solid #e4e4e4;
  ::placeholder {
    color: black;
    opacity: 1;
    text-transform: uppercase;
    font-size: 12px;
    font-family: sans-serif;
  }
`
const SubmitButton = styled(PrimaryButton)`
  display: block;
  border-radius: 0;
`
export const Subscription = () => (
  <SectionWrapper>
    <Form>
      <Input placeholder="YOUR EMAIL" />
      <SubmitButton className="btn-block">Submit</SubmitButton>
    </Form>
  </SectionWrapper>
)
