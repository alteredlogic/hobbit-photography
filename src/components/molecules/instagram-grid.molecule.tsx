import React from 'react'
import styled from '@emotion/styled'
import { Row, Col } from 'react-bootstrap'
import { SectionWrapper } from '../atoms/section-wrapper.atom'

const Column = styled(Col)`
  padding: 5px;
  img {
    width: 100%;
    height: 105px;
    object-fit: cover;
  }
`
const GridRow = styled(Row)`
  margin-right: 0x;
  margin-left: 0px;
`

export const InstagramGrid = () => (
  <SectionWrapper>
    <GridRow>
      <Column xs={4}>
        <img src="https://picsum.photos/200" />
      </Column>
      <Column xs={4}>
        <img src="https://picsum.photos/200" />
      </Column>
      <Column xs={4}>
        <img src="https://picsum.photos/200" />
      </Column>
    </GridRow>
    <GridRow>
      <Column xs={4}>
        <img src="https://picsum.photos/200" />
      </Column>
      <Column xs={4}>
        <img src="https://picsum.photos/200" />
      </Column>
      <Column xs={4}>
        <img src="https://picsum.photos/200" />
      </Column>
    </GridRow>
  </SectionWrapper>
)
