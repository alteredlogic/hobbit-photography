import React from 'react'
import styled from '@emotion/styled'
import { SectionWrapper } from '../atoms/section-wrapper.atom'
import { SocialIcon } from '../atoms/socail-icon.atom'

const IconWrapper = styled.div`
  display: flex;
  margin-left: -2px;
`

export const SocialIcons = () => (
  <SectionWrapper>
    <IconWrapper>
      <SocialIcon url="https://google.co.uk" icon="twitter" />
      <SocialIcon url="https://google.co.uk" icon="facebook" />
      <SocialIcon url="https://google.co.uk" icon="instagram" />
      <SocialIcon url="https://google.co.uk" icon="twitter" />
    </IconWrapper>
  </SectionWrapper>
)
