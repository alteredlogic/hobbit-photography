import React from 'react'
import styled from '@emotion/styled'
import { H3, H4 } from '../atoms/heading.atom'

interface Iprops {
  title: string
  subTitle: string
  sectionNumber: string
  storyText: string
  storySubText: string
  fact: string
  factTitle: string
}

const StoryWrapper = styled.div`
  color: white;
  padding: 50px 40px 120px 40px;
  position: relative;
`

const Title = styled(H3)`
  font-size: 24px;
  text-transform: uppercase;
  width: 100%;
  text-align: left;
`

const SubTitle = styled(H4)`
  font-style: italic;
  font-size: 13px;
  text-align: left;
`

const SectionNumber = styled.div`
  position: absolute;
  font-size: 18px;
  font-weight: 700;
  top: 50px;
  right: 40px;
`

const StoryText = styled.p`
  font-size: 13px;
  text-align: left;
`

const AnaliticsHolder = styled.div``

const Fact = styled(H4)``

const FactTitle = styled(H3)`
  font-style: italic;
  font-size: 13px;
`

export const MyStory = (props: Iprops) => (
  <StoryWrapper>
    <Title>{props.title}</Title>
    <SubTitle>{props.subTitle}</SubTitle>
    <SectionNumber>{props.sectionNumber}.</SectionNumber>
    <StoryText>
      <p>{props.storyText}</p>
    </StoryText>
    <StoryText>
      <p>{props.storySubText}</p>
    </StoryText>
    <AnaliticsHolder>
      <FactTitle>{props.factTitle}</FactTitle>
      <Fact>{props.fact}</Fact>
    </AnaliticsHolder>
  </StoryWrapper>
)
