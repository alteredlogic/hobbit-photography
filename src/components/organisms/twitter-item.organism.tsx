import React from 'react'
import styled from '@emotion/styled'
import { PrimaryButton } from '../atoms/button.atom'
import { SectionWrapper } from '../atoms/section-wrapper.atom'

interface Iprops {
  tweet: string
  username: string
  feedLink: string
  date: string
  displayButton?: boolean
  hasNoTitle?: boolean
}

interface ITitle {
  hasNoTitle?: boolean
}
const TweetWrapper = styled('div')<ITitle>`
  font-size: 12px;
  margin-top: ${props => (props.hasNoTitle ? '-30px' : '0px')};
  padding-bottom: 10px;
  text-align: left;
  color: #888;
  a {
    color: white;
  }
`
const Username = styled.a`
  font-size: 12px;
`
const FeedLink = styled.p`
  font-size: 12px;
`
const Date = styled.p`
  font-size: 12px;
  font-style: italic;
`
const Button = styled(PrimaryButton)`
  padding: 4px 10px;
  font-size: 10px;
  margin-right: 10px;
`
const FollowButton = styled(PrimaryButton)`
  padding: 15px 15px;
  font-size: 10px;
  margin-right: 10px;
  margin-top: 20px;
  width: 100%;
`

export const TwitterItem = (props: Iprops) => (
  <SectionWrapper>
    <TweetWrapper hasNoTitle={props.hasNoTitle}>
      <Username>{props.username}</Username>
      <FeedLink>{props.feedLink}</FeedLink>
      <Date>Posted on {props.date}</Date>
      <div>
        <Button>Reply</Button>
        <Button>Retweet</Button>
        <Button>Favourite</Button>
      </div>
      {props.displayButton && <FollowButton>Follow Us</FollowButton>}
    </TweetWrapper>
  </SectionWrapper>
)
