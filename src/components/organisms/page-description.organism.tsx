import React, { ReactElement, SFC } from 'react'
import styled from '@emotion/styled'
import { H4, H2 } from '../atoms/heading.atom'

interface IProps {
  backgroundImageUrl: string
  title: string
  text: string
}

const Wrapper = styled.section<{ backgroundImageUrl: string }>`
  position: fixed;
  width: 50%;
  height: 100%;
  background: url(${props => props.backgroundImageUrl}) no-repeat;
  background-size: cover;
  background-clip: border-box;
`
const FixedPhotoDecoration = styled.div`
  position: absolute;
  top: 50px;
  right: 50px;
  width: 70px;
  height: 70px;
  z-index: 21;
  border-top: 1px solid;
  border-right: 1px solid;
  border-color: rgba(255, 255, 255, 0.51);
`
const TextBlock = styled.div`
  position: absolute;
  bottom: 140px;
  left: 0;
  width: 100%;
  text-align: left;
  padding: 20px 50px;
  z-index: 11;
`
export const PageDescription: SFC<IProps> = ({
  backgroundImageUrl,
  title,
  text
}): ReactElement => (
  <Wrapper backgroundImageUrl={backgroundImageUrl}>
    <FixedPhotoDecoration />
    <TextBlock>
      <H2>{title}</H2>
      <H4>{text}</H4>
    </TextBlock>
  </Wrapper>
)
