import React from 'react'
import styled from '@emotion/styled'
import { H3, H4 } from '../atoms/heading.atom'
import { Subscription } from '../molecules/subscription.molecule'
import { InstagramGrid } from '../molecules/instagram-grid.molecule'
import { SocialIcons } from '../molecules/socail-row.molecule'
import { TwitterItem } from './twitter-item.organism'

interface IProps {
  isOpen: boolean
}

const SidebarWrapper = styled.div<IProps>`
  position: absolute;
  z-index: 1;
  top: 90px;
  right: 0px;
  width: 400px;
  background-color: #1b1b1b;
  padding: 50px 40px;
  transition: transform 0.3s ease-in-out;
  transform: ${(props: IProps) =>
    props.isOpen ? 'translateX(0)' : 'translateX(400px)'};
`
const Heading = styled(H3)`
  font-size: 12px;
  text-transform: uppercase;
`
const Paragraph = styled.p``

export const SideBar = ({ isOpen }: IProps) => (
  <SidebarWrapper isOpen={isOpen}>
    <Heading>SUBSCRIBE TO OUR NEWSLETTER</Heading>
    <H4>
      Lorem ipsum dosectetur adipisicing elit, sed do.Lorem ipsum dolor sit
      amet,
    </H4>
    <Subscription />
    <Heading>INSTAGRAM PHOTOS</Heading>
    <InstagramGrid />
    <Heading> We are social</Heading>
    <div>
      <SocialIcons />
    </div>
    <Heading>Our latest Tweets</Heading>
    <TwitterItem
      tweet=""
      username="@Jonny"
      feedLink="Check out 'Townhub - Directory Listing Template' on #EnvatoMarket by
      @katokli3mmm #themeforest
      
      <a href='https://themeforest.net/item/townhub-directory-listing-template/25706166?utm_source=sharetw'> am i an ankor </a>"
      date="feb 2nd"
    />
    <TwitterItem
      tweet=""
      username="@Jonny"
      feedLink="Check out 'Townhub - Directory Listing Template' on #EnvatoMarket by
      @katokli3mmm #themeforest
      
      <a href='https://themeforest.net/item/townhub-directory-listing-template/25706166?utm_source=sharetw'> am i an ankor </a>"
      date="feb 2nd"
      displayButton
      hasNoTitle
    />
  </SidebarWrapper>
)
