import React from 'react'
import styled from '@emotion/styled'
import Navbar from 'react-bootstrap/Navbar'
import NavItem from 'react-bootstrap/NavItem'
import Nav from 'react-bootstrap/Nav'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShareAlt, faEllipsisV } from '@fortawesome/free-solid-svg-icons'
import { LinkContainer } from 'react-router-bootstrap'

const Navigation = styled(Navbar)`
  background-color: #161616;
  height: 90px;
  padding: 0 60px;
  > .nav-link {
    color: white;
  }
`

const Link = styled(NavItem)`
  &&& {
    color: white;
    text-transform: uppercase;
    font-size: 12px;
    transition: all 100ms linear;
    :hover {
      color: #999;
    }
    font-family: 'Ek Mukta';
    font-weight: bold;
    padding: 10px;
    cursor: pointer;
  }
`
const Icon = styled(FontAwesomeIcon)`
  font-size: 19px;
  margin-left: 5px;
`
const Brand = styled(Navbar.Brand)`
  &&& {
    color: white;
  }
`
interface IProps {
  setIsOpen: (toggle: boolean) => void
  isOpen: boolean
}
export const Header = (props: IProps) => (
  <Navigation expand="lg" fixed="top">
    <Brand href="#home">Imagination Photography</Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="navbar-nav ml-auto">
        <LinkContainer to="/">
          <Link>Home</Link>
        </LinkContainer>
        <LinkContainer to="/portfolio">
          <Link>Portfolio</Link>
        </LinkContainer>
        <LinkContainer to="/about">
          <Link>About</Link>
        </LinkContainer>
        <LinkContainer to="/news">
          <Link>News</Link>
        </LinkContainer>
        <LinkContainer to="/contact">
          <Link>Contact</Link>
        </LinkContainer>
      </Nav>
      <Nav className="navbar-nav">
        <Link href="#home">
          <Icon icon={faShareAlt} />
        </Link>
        <Link href="#link">
          <Icon
            icon={faEllipsisV}
            onClick={() => props.setIsOpen(!props.isOpen)}
          />
        </Link>
      </Nav>
    </Navbar.Collapse>
  </Navigation>
)
