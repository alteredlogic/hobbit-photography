import React from 'react'
import { storiesOf } from '@storybook/react'
import { H3, H4 } from '../components/atoms/heading.atom'

storiesOf('Atoms/Headings', module)
  .add('H3', () => {
    return <H3>CONTACT INFORMATION</H3>
  })
  .add('H4', () => {
    return (
      <H4>
        Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa
      </H4>
    )
  })
