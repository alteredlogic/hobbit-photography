import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { PrimaryButton, SecondaryButton } from '../components/atoms/button.atom'

storiesOf('Atoms/Buttons', module)
  .add('Primary Button', () => {
    return (
      <PrimaryButton onClick={action('Back button click')}>
        Primary Button
      </PrimaryButton>
    )
  })
  .add('Secondary Button', () => {
    return (
      <SecondaryButton onClick={action('Back button click')}>
        Secondary Button
      </SecondaryButton>
    )
  })
