import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { PrimaryButton, SecondaryButton } from '../components/atoms/button.atom'
import { SocialIcon } from '../components/atoms/socail-icon.atom'

storiesOf('Atoms/SocailIcon', module)
  .add('Twitter Icon', () => {
    return <SocialIcon icon="twitter" />
  })
  .add('Facebook Icon', () => {
    return <SocialIcon icon="facebook" />
  })
