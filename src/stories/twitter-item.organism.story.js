import React from 'react'
import { storiesOf } from '@storybook/react'
import { H3, H4 } from '../components/atoms/heading.atom'
import {
  title,
  username,
  feed,
  date
} from '../components/organisms/twitter-item.organism'

storiesOf('twiter-item.organism', module)
  .add('title', () => {
    return <title>Test</title>
  })
  .add('usermane', () => {
    return <username>Test</username>
  })
  .add('feed', () => {
    return <feed>Test</feed>
  })
  .add('date', () => {
    return <date>Test</date>
  })
