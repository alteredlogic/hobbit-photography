export const theme = {
  colors: {
    gold: '#A6735E',
    white: '#ffffff',
    brown: '#3E3E3E'
  },
  fonts: {
    regular: 'galano_grotesqueregular',
    medium: 'galano_grotesquemedium',
    bold: 'galano_grotesquebold'
  }
}

export const mediaQueries = {
  desktop: '1024px',
  largeDesktop: '1366px',
  tablet: '660px'
}
