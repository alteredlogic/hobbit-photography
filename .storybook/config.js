import { configure, addParameters } from '@storybook/react'
import '../src/index.css'
import './storybook.css'
const req = require.context('../src', true, /.story.js/)

configure(() => {
  req.keys().forEach(filename => req(filename))
  addParameters({
    viewport: {
      viewports: {
        name: 'Responsive',
        type: 'desktop',
        styles: {
          height: '100%',
          width: '100%',
        },
      },
    },
  })
}, module)
